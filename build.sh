#!/bin/bash
preamble='//Licensed_under_Apache_2.0,_copyright_Philippe_Monnaie_2017._See_https://gitlab.com/pmon/gitlab-api-js'
rm -rf dist/*
browserify  --entry --standalone gitlab src/gitlab-api.js > dist/gitlab-api.js
uglifyjs --compress --mangle --preamble $preamble -- dist/gitlab-api.js > dist/gitlab-api.min.js
