### Overview
First of all, thanks for your interest in helping make gitlab-api-js even better. Contributions help resolve rare bugs, accomplish neat new features, polish the code, and improve the documentation.

### Guidelines for contributing
- Planning to use QUnit for tests
- Suggestions on improvements are welcome
- TODO list in README.md