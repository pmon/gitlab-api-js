// git-api-frontend, copyright (c) by Philippe Monnaie and others
// Distributed under an Apache 2.0 license: https://gitlab.com/pmon/gitlab-api-js/LICENSE.txt

"use strict";

/*
Commandline for quick reference:
user=<your_user>
password=<your_password>
clear #you put your password in your bash history, but at least you removed it from your terminal
baseurl=https://gitlab.com
curl $baseurl/api/v3/session --data-urlencode "login=$user" --data-urlencode "password=$password"
private_token=<your_privte_token> #get this from the json response
curl $baseurl/api/v3/projects?private_token=$token
project_id=<your_project_id> #pick one from your projects (you need to owner to see this)
filepath=<your_test_file_path>

curl "$baseurl/api/v3/projects/$project_id/repository/files?file_path=$filepath&ref=master&private_token=$token"

curl --request POST $baseurl/api/v3/projects/$project_id/fork --data-urlencode private_token=$token

# Create file
curl --request POST --data-urlencode "private_token=$token" --data-urlencode "content=Test file\n upload content" --data-urlencode "commit_message=Create file via api test" "$baseurl/api/v3//projects/$project_id/repository/files?file_path=$filepath&branch_name=master&author_email=$email"

#Update file
curl --request PUT --data-urlencode "private_token=$token" --data-urlencode "content=Test file\n upload content" --data-urlencode "commit_message=Create file via api test" "$baseurl/api/v3//projects/$project_id/repository/files?file_path=$filepath&branch_name=master&author_email=$email"

*/
	
function gitlab(url){
	//global variables that should be configurable somewhere
	var gitUrl = url
	var gitApiUrl = gitUrl + 'api/v3/'
	
	var callGitLab = function callGitLab(path, method, args = {}, callback, errorcallback=defaultErrorCallback){
		var url = gitApiUrl + path;
		$.ajax({
			type: method,
			url: url,
			crossDomain: true,
			data: args,
			dataType: 'json',
			success: function(responseData, textStatus, jqXHR) {
				callback(responseData, textStatus, jqXHR);
			},
			error: function (responseData, textStatus, errorThrown) {
				errorcallback(responseData, textStatus, errorThrown);
			}
		});
	}
	
	var defaultAjaxCallback = function(responseData, textStatus, jqXHR){
		console.log(responseData);
		console.log(textStatus);
		console.log(jqXHR);
	}
	
	var defaultErrorCallback = defaultAjaxCallback;
	
	var loginGitLab = function loginGitLab(callback, errorCallback=defaultErrorCallback, username, password){
		var credentials = {'login': username, 'password': password};
		var path = 'session';
		callGitLab(
			path, 
			'POST',
			credentials,
			callback,
			errorCallback
		);
	}
	
	var getApiProjectsPath = function(projectId){
		return 'projects/' + projectId + '/';
	}
	
	var getApiFilesPath = function(projectId){
		return getApiProjectsPath(projectId) + 'repository/files';
	}
	
	var getFileContents = function getFileContents(projectId, filePath, token, branch='master', callback, errorCallback=defaultErrorCallback){
		var path = getApiFilesPath(projectId);
		var data = {'file_path': filePath, 'ref': branch, 'private_token': token};
		callGitLab(
			path, 
			'GET',
			data,
			function(result, textStatus, jqXHR){
				var file_contents = atob(result.content);
				callback(file_contents, result, textStatus, jqXHR);
			},
			errorCallback
		);
	}
	
	var forkProject = function forkProject(projectId, token){
		var path = getApiProjectsPath(projectId) + 'fork';
		callGitLab(
			path,
			'POST',
			data,
			function(result, textStatus, jqXHR){
				//TODO
			},
			function(responseData, textStatus, errorThrown){
		 
			}
		)
	}
	
	var mergeProject = function mergeProject(projectId, token){
	
	}
	
	var commitToGit = function(method, projectId, filePath, contents, message, token, email, branchName='master', callback=defaultAjaxCallback, errorcallback=defaultErrorCallback, encoding){
		var path = getApiFilesPath(projectId);
		var data = {'file_path': filePath, 'branch_name': branchName, 'private_token': token, 'content': contents, 'commit_message': message, 'author_email': email};
		if(encoding){
			data['encoding'] = encoding
		}
		callGitLab(
			path,
			method,
			data,
			function(result, textStatus, jqXHR){
				callback(result, textStatus, jqXHR);
			},
			function(responseData, textStatus, errorThrown){
				errorcallback(responseData, textStatus, errorThrown);
			}
		)	
	}
	
	var commitUpdateToGit = function(projectId, filePath, contents, message, token, email, branchName='master', callback=defaultAjaxCallback, errorcallback=defaultErrorCallback, encoding){
		commitToGit('PUT', projectId, filePath, contents, message, token, email, branchName, callback, errorcallback, encoding)
	}
	
	var commitNewFileToGit = function(projectId, filePath, contents, message, token, email, branchName='master', callback=defaultAjaxCallback, errorcallback=defaultErrorCallback, encoding){
		commitToGit('POST', projectId, filePath, contents, message, token, email, branchName, callback, errorcallback, encoding)
	}
	
	return {
		loginGitLab: loginGitLab,
		getFileContents: getFileContents,
		commitToGit: commitToGit,
		commitUpdateToGit: commitUpdateToGit,
		commitNewFileToGit: commitNewFileToGit,
		forkProject: forkProject,
		mergeProject: mergeProject
	}
}

module.exports = gitlab;