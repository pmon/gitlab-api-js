# Gitlab-api-js

gitlab-api-js is a library that encapsulates the gitlab api to be used from a javascript frontend. 

Currently still a work in progress (some functions are still in a TODO state). Very little has been well tested.

For license see {License.txt}

# Prerequisites

Currently needs jquery (planning to remove that dependency).

# TODO

 - Deploy on npm
 - Fix package.json
 - Remove dependency on jquery (should only be for ajax calls)
 - Add functionality for fork/merge/delete
 - Add tests
 - Setup build process (run tests + minify)
 - Generalize framework and add other git APIs (github, bitbucket, etc..). We should keep the same functions and arguments where possible.
 - Make function arguments some kind of list instead of the current mess to allow for easy optional arguments etc